import json
from geopy import geocoders
from time import sleep

with open("data.json") as f:
	with open("dat.json", "a") as o:
		for line in f:
			data=json.loads(line)
			g = geocoders.Google(domain='maps.google.co.uk')
			place, (lat, lng) = g.geocode(data["name"])
			del data["price"]
			data["lat"] = lat
			data["lng"] = lng
			o.write(json.dumps(data) + "\n")
			sleep(0.3)
