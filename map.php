<?php
	function geocode($address) {
		if (strpos(strtolower($address), 'edinburgh') === false){
			$address = "$address, Edinburgh";
		}
		$string = str_replace (" ", "+", urlencode($address));
		$details_url = "http://maps.googleapis.com/maps/api/geocode/json?address=".$string."&sensor=false";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $details_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$response = json_decode(curl_exec($ch), true);

		// If Status Code is ZERO_RESULTS, OVER_QUERY_LIMIT, REQUEST_DENIED or INVALID_REQUEST
		if ($response['status'] != 'OK') {
			return '55.944185, -3.187538';
		}

		$geometry = $response['results'][0]['geometry'];

		$longitude = $geometry['location']['lng'];
		$latitude = $geometry['location']['lat'];

		$array = array(
		'latitude' => $geometry['location']['lat'],
		'longitude' => $geometry['location']['lng'],
		'location_type' => $geometry['location_type'],
		);

		return "$latitude, $longitude";
	}

?>
<!DOCTYPE html>
<html>
        <head>
                <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
                <title>Park-o-matic</title>
                <link rel="stylesheet" type="text/css" href="/css/main.css"/>
                <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
                <!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDkGT26HoUeyklHBsm2TuiNvKNyo89YV6I&sensor=false"></script>-->
                <script type="text/javascript" src="/js/carparks.js"></script>
                <script type="text/javascript" src="/js/geocode.js"></script>
		<script type="text/javascript" src="/js/distancematrix.js"></script>
                <script type="text/javascript" src="/js/main.js"></script>
        </head>
        <body onload="initialise(<?php echo geocode($_POST['address']); ?>)">
                <div class="content">
                        <a href="/"><h1>Park-o-matic</h1></a>
                        <p>Parking options near to <?php echo ucwords($_POST['address']); ?> </p>
                        <div id="map_canvas" style="width:800px; height:600px; margin:0 auto; color:black; text-align: left"></div>
                </div>
        </body>
</html>

