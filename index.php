<!DOCTYPE html>
<html>
	<head>
		<title>Park-o-Matic</title>
		<link rel="stylesheet" type="text/css" href="./css/main.css"/>
	</head>
	<body>
		<div class="content">
			<h1>Park-o-Matic</h1>
		<p>The easy way to park in Edinburgh</p> 
			<form method="post" id="searchbox" action="./map.php">
				<input type="text" name="address" placeholder="Near me"/>
				<button type="submit">Find Parking!</button>
			</form>
		</div>
	</body>
</html>
