from bs4 import BeautifulSoup

import urllib.request

url = urllib.request.urlopen("http://edinburgh.cdmf.info/public/onstreet/list.htm")
mybytes = url.read()

mystr = mybytes.decode("utf8")

url.close()

soup = BeautifulSoup(mystr)

main_body = soup.tbody

lines = main_body.find_all('tr')
# print(len(list(main_body)))
# print(lines)
#print(len(lines))
#print(lines[0])
first_line = lines[0].find_all('td')
#print(first_line)
#for td in first_line:
#    print(td.string)


#for child in (first_line[1].children):
#    print(child)
#print(len(list(first_line[1].children)))
#print(first_line[1].span.string)
txt_file = open("data.txt", "w")
txt_file.write("Street"+'\t'+'\t'+"cost"+'\t'+"Stay"+'\t'+"Places"+'\n')
for line in lines:
    outline = ""
    td = line.find_all('td')
    
    outline += td[1].span.string + '\t' + '\t'
    outline += td[2].string + '\t'
    outline += td[3].string + '\t'
    places = int(td[4].string) + int(td[5].string)
    outline += str(places) + '\n'
    txt_file.write(outline)

txt_file.close()


