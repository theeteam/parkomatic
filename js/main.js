function initialise(lat, lng) {
	window.lat = lat;
	window.lng = lng;
	var mapOptions = {
		center: new google.maps.LatLng(lat, lng),
		zoom: 17,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	map = new google.maps.Map(document.getElementById("map_canvas"),mapOptions);

	var marker = new google.maps.Marker({
		position: new google.maps.LatLng(lat, lng),
		map: map
	}); 

	addClosest();
	addMarkers();
}

function addMarkers() {
	var infowindow = new google.maps.InfoWindow({
			
		});

	carparks.forEach(function(mark) {

		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(mark['lat'], mark['lng']),
			map: map,
			title: mark.name + ", " + "pph:" + mark.pph,
			icon:'http://www.reide96.com/ilwhack/images/parking-icon.png'
		}); 
		
		google.maps.event.addListener(marker, 'click', function() {
			infowindow.setContent (mark.name + '<br>' + mark.places + " spaces" + '<br>' + "£" + mark.pph + '<br>' + mark.stay + " hours")
			infowindow.open(map, marker);
		});
	});
}


function addClosest(){
	var marker = new google.maps.Marker({
	  position: new google.maps.LatLng(window.closestCarPark['lat'], window.closestCarPark['lng']),
      map: map,
      title: "mark",
      icon: 'www.reide96.com/ilwhack/images/closestparking-icon.png'
	})


}

