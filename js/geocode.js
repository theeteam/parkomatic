function getLatLong(address, callback) {
	var geocoder = new google.maps.Geocoder();
	var result = "";
	geocoder.geocode( { 'address': address, 'region': 'uk' }, callback);	
};

function distHaversine (lat1, lon1, lat2, lon2) {
  	var R = 6371; // earth's mean radius in km
  	var dLat = (lat2-lat1).toRad();
  	var dLon = (lon2-lon1).toRad();
  	lat1 = lat1.toRad(), lat2 = lat2.toRad();

  	var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
  	        Math.cos(lat1) * Math.cos(lat2) * 
    	      	Math.sin(dLon/2) * Math.sin(dLon/2);
 	 var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
  	var d = R * c;
  	var d2= d * 3280.8399; 
  	return d;
}

getLatLong("Informatics Forum, Edinburgh", function(results, status) {
    	if (status == google.maps.GeocoderStatus.OK) {
		console.log('I got something!');
		console.log("Lat : " + results[0].geometry.location.lat());
		console.log("Lng : " + results[0].geometry.location.lng());
    	} else {
		console.log('I did not get something!');
	}
});
