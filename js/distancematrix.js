var origin1 = new google.maps.LatLng(window.lat, window.lng);
var destination = [];
//Construct array destination containing 
for (i = 0; i < carparks.length; i++ ){
	destination.push(new google.maps.LatLng(carparks[i]['lat'], carparks[i]['lng']));
}

var service = new google.maps.DistanceMatrixService();
service.getDistanceMatrix(
  {
    origins: [origin1],
    destinations: destination,
    travelMode: google.maps.TravelMode.DRIVING,
    avoidHighways: false,
    avoidTolls: false
  }, 
  window.closestCarPark = callback);



function callback(response, status) {
  if (status == google.maps.DistanceMatrixStatus.OK) {
    var origins = response.originAddresses;
    var destinations = response.destinationAddresses;

    for (var i = 0; i < origins.length; i++) {
      var results = response.rows[i].elements;
      for (var j = 0; j < results.length; j++) {
        var element = results[j];
        var distance = element.distance.text;
        var duration = element.duration.text;
        var from = origins[i];
        var to = destinations[j];
      }
      var minimum = 0;
      var mindist = 100000000;
      for (var j = 0; j < results.length; j++) {
          if (results[j].distance < mindist){
            minimum = j;
            mindist = results[j].distance;
          }
      }
    }
    return results[minimum];
  }
}

